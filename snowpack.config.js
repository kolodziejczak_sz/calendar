/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
    alias: {
        '@root': './src',
    },
    devOptions: {
        port: 3000,
    },
    mount: {
        public: { url: '/' },
        src: { url: '/dist' },
    },
    optimize: {
        bundle: true,
        minify: true,
        target: 'es2018',
    },
    out: 'build',
    plugins: [
        '@snowpack/plugin-sass',
        '@snowpack/plugin-react-refresh',
        '@snowpack/plugin-dotenv',
        '@snowpack/plugin-typescript',
    ],
};
