# Zadanie - Front-End Developer

Utwórz widok kalendarza, który składa się z widoku dnia wraz z pogodą oraz z widoku miesiąca (siatka).
Można się wzorować z podobnych rozwiązań (np. kalendarz Google),
ale nie można wykorzystywać gotowych elementów (np. widgetów).
Widok powinien być przygotowany w jednym z trzech frameworków: Vue, Angular lub React.

### Widok dnia:

-   powinien zawierać listę wydarzeń z danego dnia (mogą być losowe)
-   poukładane wg kolejności godzin
-   powinien nad listą wydarzeń prezentować podstawowe informacje o pogodzie pobierane poprzez API (np. OpenWeather)
-   powinien umożliwiać dodanie nowego lub edycję wydarzenia w kalendarzu (godz. od- do + nazwa)
-   informacje do kalendarza powinny być przechowywane w lokalnym “storage” danego frameworka (nie w backend)
-   powinna być możliwość zmienienia dnia na następny i poprzedni

### Widok miesiąca:

-   widok miesiąca powinien być pokazany w postaci siatki (grid) cały miesiąc na raz
-   widok w każdym dniu powinien pokazywać liczbę wydarzeń (bez dodatkowych informacji)
-   powinna być możliwość zmienienia miesiąca na poprzedni i następny
-   po kliknięciu w dzień na siatce użytkownik powinien przejść do widoku dnia

## Co należy zrobić:

-   przygotować projekt w wybranym frameworku
-   przygotować instrukcję budowania projektu
-   przygotować stronę z przyciskami dzień / miesiąc by móc wybrać widok (przyciski zawsze widoczne)
-   przygotować własny panel z informacją o pogodzie na podstawie danych z API
-   przygotować widoki dnia i miesiąca
-   przygotować widok dodawania/edycji wydarzenia
-   strona powinna zawierać podstawowe style (CSS) ale kolorystyka czy dobór stylu nie będzie oceniany
-   strona powinna być w stylu SPA-strona powinna być utworzona zgodnie z zasadami RWDO
-   ceniana będzie umiejętność programowania nie umiejętności stylistyczne !!!
