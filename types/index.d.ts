interface ImportMeta {
    hot: {
        accept: () => void;
    };
    env: {
        [key: string]: any;
    };
}
