module.exports = {
    plugins: ['stylelint-scss'],
    extends: ['stylelint-config-standard'],
    rules: {
        'no-empty-source': null,
        'declaration-empty-line-before': null,
        'no-missing-end-of-source-newline': null,
        indentation: 4,
        'at-rule-no-unknown': null,
        'scss/at-rule-no-unknown': true,
        'rule-empty-line-before': 'always',
        'at-rule-empty-line-before': 'never',
        'selector-type-case': 'lower',
        'color-named': 'never',
        'color-hex-length': 'short',
        'color-hex-case': 'lower',
        'declaration-colon-newline-after': null,
        'length-zero-no-unit': [true, { ignore: ['custom-properties'] }],
        'max-empty-lines': [1, { ignore: ['comments'] }],
        'block-closing-brace-newline-after': 'always-single-line',
        'no-eol-whitespace': null,
    },
};
