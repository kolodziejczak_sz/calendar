import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { render } from '@root/test/render';
import { Day } from './Day.component';

describe('<Day>', () => {
    it('Should render correctly', async () => {
        render(<Day />);

        expect(await screen.findByTestId('eventList')).toBeInTheDocument();

        const newEventEl = await screen.findByTitle('eventNew');
        userEvent.click(newEventEl);

        expect(await screen.findByTestId('eventForm')).toBeInTheDocument();
    });
});
