import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IconButton } from '@root/common/components';
import { SelectedDate } from '@root/modules/calendar';
import {
    actions as eventActions,
    EventForm,
    EventList,
    newEventId,
    noEventId,
    selectSelectedEventId,
} from '@root/modules/events';
import { FormattedMessage } from '@root/modules/intl';
import { DayWeather } from '@root/modules/weather';
import './Day.styles.scss';

export const Day = () => {
    const dispatch = useDispatch();
    const selectedEventId = useSelector(selectSelectedEventId);
    const addHandler = useCallback(
        () => dispatch(eventActions.eventSelected(newEventId)),
        []
    );

    return (
        <div className="Day" data-testid="day">
            <header>
                <SelectedDate className="Day__title" />
            </header>
            <DayWeather />
            {selectedEventId !== noEventId ? (
                <EventForm className="Day__form" />
            ) : (
                <>
                    <div className="Day__newEvent">
                        <FormattedMessage id="eventNew" />{' '}
                        <IconButton
                            className="Day__button"
                            name="add"
                            onClick={addHandler}
                            title="eventNew"
                        />
                    </div>
                    <EventList />
                </>
            )}
        </div>
    );
};
