import React from 'react';
import { screen } from '@testing-library/react';
import { render } from '@root/test/render';
import { Nav } from './Nav.component';

describe('<Nav>', () => {
    it('Should render correctly', async () => {
        render(<Nav />);

        const elements = [
            await screen.findByTestId('prevPeriod'),
            await screen.findByTitle('monthView'),
            await screen.findByTitle('dayView'),
        ];

        elements.forEach((el) => {
            expect(el).toBeDefined();
        });
    });
});
