import React from 'react';
import cx from 'classnames';
import { useSelector, useDispatch } from 'react-redux';
import { IconButton } from '@root/common/components/IconButton';
import {
    CalendarView,
    selectCalendarView,
    actions as calendarActions,
    DateSelector,
} from '@root/modules/calendar';
import './Nav.styles.scss';

export const Nav = () => {
    const calendarView = useSelector(selectCalendarView);
    const isMonthView = calendarView === CalendarView.MONTH;

    const dispatch = useDispatch();
    const navigate = (view: CalendarView) => () => {
        dispatch(calendarActions.viewChanged(view));
    };

    return (
        <nav className="Nav">
            <div className="Nav__container">
                <div className="Nav__links">
                    <IconButton
                        className={cx({ Nav__active: isMonthView })}
                        title="monthView"
                        name="calendarMonth"
                        onClick={navigate(CalendarView.MONTH)}
                    />
                    <IconButton
                        className={cx({ Nav__active: !isMonthView })}
                        title="dayView"
                        name="calendarDay"
                        onClick={navigate(CalendarView.DAY)}
                    />
                </div>
                <DateSelector />
            </div>
        </nav>
    );
};
