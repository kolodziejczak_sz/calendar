import React from 'react';
import { useSelector } from 'react-redux';
import { selectCalendarView, CalendarView } from '@root/modules/calendar';
import { EventCalendar } from '@root/modules/events';
import { Day } from './components/Day';
import { Nav } from './components/Nav';
import './CalendarPage.styles.scss';

export const CalendarPage = () => {
    const calendarView = useSelector(selectCalendarView);
    const Element = {
        [CalendarView.MONTH]: EventCalendar,
        [CalendarView.DAY]: Day,
    }[calendarView];

    return (
        <div className="CalendarPage">
            <Nav />
            <main className="CalendarPage__outlet">
                <Element />
            </main>
        </div>
    );
};
