import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { render } from '@root/test/render';
import { formatToDate, now } from '@root/common/utils/date';
import { CalendarPage } from './CalendarPage';

describe('<CalendarPage>', () => {
    it('Should render correctly', async () => {
        render(<CalendarPage />);

        const todayCellEl = await screen.findByTitle(formatToDate(now()));
        userEvent.click(todayCellEl);

        expect(await screen.findByTestId('day')).toBeInTheDocument();
    });
});
