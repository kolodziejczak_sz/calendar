import { EnhancedStore } from '@reduxjs/toolkit';
import { saveState } from './stateStorage';

export const persist = (store: EnhancedStore) => {
    const unsub = store.subscribe(() => {
        const state = store.getState();
        saveState(state);
    });

    document.addEventListener('beforeunload', unsub, { once: true });
};
