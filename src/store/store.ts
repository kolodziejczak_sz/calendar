import { configureStore } from '@reduxjs/toolkit';
import { reducer as eventsReducer } from '@root/modules/events';
import { reducer as calendarReducer } from '@root/modules/calendar';
import {
    reducer as weatherReducer,
    weatherService,
} from '@root/modules/weather';
import { getInitialState } from './initialState';
import { persist } from './persist';

export const rootReducer = {
    calendar: calendarReducer,
    events: eventsReducer,
    weather: weatherReducer,
};

export const createStore = () => {
    const preloadedState = getInitialState();
    const store = configureStore({
        reducer: rootReducer,
        preloadedState,
        middleware: (getDefaultMiddleware) =>
            getDefaultMiddleware({
                thunk: {
                    extraArgument: weatherService,
                },
            }),
    });

    persist(store);

    return store;
};

export const store = createStore();

export type AppState = {
    calendar: ReturnType<typeof calendarReducer>;
    events: ReturnType<typeof eventsReducer>;
    weather: ReturnType<typeof weatherReducer>;
};

export type ThunkAPI = {
    dispatch: typeof store.dispatch;
    state: AppState;
    extra: typeof weatherService;
};
