import { now, roundToNearestMinutes } from '@root/common/utils/date';
import { initialState as CalendarInitialState } from '@root/modules/calendar';
import { loadState } from './stateStorage';
import { AppState } from './store';

const firstEventTimestamp = Number(
    roundToNearestMinutes(now(), { nearestTo: 5 })
);

const newUserState: Partial<AppState> = {
    events: {
        selectedId: undefined,
        ids: ['1'],
        entities: {
            '1': {
                id: '1',
                name: 'Hello world',
                startTimestamp: firstEventTimestamp,
                endTimestamp: firstEventTimestamp + 60_000 * 5,
            },
        },
    },
};

export const getInitialState: () => Partial<AppState> = () => {
    const storedState = loadState();

    return {
        ...(storedState || newUserState),
        calendar: CalendarInitialState,
        weather: undefined,
    };
};
