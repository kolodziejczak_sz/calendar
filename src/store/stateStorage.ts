import { AppState } from './store';

const localStorageKey = '__CALENDAR_APP_STATE__';

export const saveState = (state: AppState) => {
    try {
        const stringifiedState = JSON.stringify(state);
        localStorage.setItem(localStorageKey, stringifiedState);
    } catch (err) {
        // sentry
    }
};

export const loadState: () => AppState | null = () => {
    try {
        const stringifiedState = localStorage.getItem(localStorageKey);
        const state = JSON.parse(stringifiedState);
        return state;
    } catch (err) {
        // sentry
        return null;
    }
};
