import { createStore } from '../store';
import { loadState, saveState } from '../stateStorage';

jest.mock('../stateStorage');

describe('persist', () => {
    beforeEach(() => jest.clearAllMocks());

    it('should load state from storage at the time of store creation', () => {
        createStore();
        expect(loadState).toBeCalledTimes(1);
    });

    it('should save state to storage after any action', () => {
        const store = createStore();
        expect(saveState).toBeCalledTimes(0);

        store.dispatch({ type: 'anyAction' });

        expect(saveState).toBeCalledTimes(1);
        expect(saveState).toBeCalledWith(store.getState());
    });
});
