import React, { HTMLProps } from 'react';
import cx from 'classnames';
import { IconName } from '@root/common/icons';
import { Icon } from '@root/common/components';
import { useIntl } from '@root/modules/intl';
import './IconButton.styles.scss';

type ButtonTypes = 'button' | 'submit' | 'reset';

interface IconButtonProps extends HTMLProps<HTMLButtonElement> {
    name: IconName;
    title: string;
    type?: ButtonTypes;
}

export const IconButton = ({
    name,
    type = 'button',
    title,
    className,
    ...props
}: IconButtonProps) => {
    const intl = useIntl();
    return (
        <button
            name={name}
            className={cx('IconButton', className)}
            title={intl.formatMessage({ id: title })}
            type={type}
            {...props}
        >
            <Icon name={name} />
        </button>
    );
};
