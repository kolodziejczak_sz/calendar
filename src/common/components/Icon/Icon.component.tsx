import React from 'react';
import { IconName, icons } from '@root/common/icons';
import './Icon.styles.scss';

interface IconProps {
    name: IconName;
    className?: string;
}

export const Icon = ({ name }: IconProps) => {
    const SvgIcon = icons[name];

    return <SvgIcon />;
};
