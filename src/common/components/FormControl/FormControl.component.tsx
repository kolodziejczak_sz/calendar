import React, { HTMLProps } from 'react';
import cx from 'classnames';
import { useIntl } from '@root/modules/intl';
import './FormControl.styles.scss';

interface FormControlProps extends HTMLProps<HTMLInputElement> {
    title: string;
}

export const FormControl = ({
    className,
    title,
    ...rest
}: FormControlProps) => {
    const intl = useIntl();
    const translatedTitle = intl.formatMessage({ id: title });

    return (
        <input
            title={translatedTitle}
            placeholder={translatedTitle}
            className={cx('FormControl', className)}
            {...rest}
        />
    );
};
