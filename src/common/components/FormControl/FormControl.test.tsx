import React from 'react';
import { render } from '@root/test/render';
import { FormControl } from './FormControl.component';

describe('<FormControl>', () => {
    it('renders without error', () => {
        const { asFragment } = render(<FormControl title="any" />);

        expect(asFragment()).toMatchInlineSnapshot(`
            <DocumentFragment>
              <input
                class="FormControl"
                placeholder="any"
                title="any"
              />
            </DocumentFragment>
        `);
    });
});
