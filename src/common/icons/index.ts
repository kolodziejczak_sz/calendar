import add from './Add';
import calendarDay from './CalendarDay';
import calendarMonth from './CalendarMonth';
import cancel from './Cancel';
import clock from './Clock';
import navigateBefore from './NavigateBefore';
import navigateNext from './NavigateNext';
import remove from './Remove';
import save from './Save';

export const icons = {
    add,
    calendarDay,
    calendarMonth,
    cancel,
    clock,
    navigateBefore,
    navigateNext,
    remove,
    save,
} as const;

export type IconName = keyof typeof icons;
