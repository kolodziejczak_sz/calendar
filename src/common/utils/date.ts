import { format } from 'date-fns';
export {
    addMinutes,
    addMonths,
    addDays,
    startOfWeek,
    endOfWeek,
    endOfMonth,
    isSameDay,
    isSameMonth,
    eachDayOfInterval,
    roundToNearestMinutes,
    set,
} from 'date-fns';

const DATE_FORMAT = 'yyyy-MM-dd';
const TIME_FORMAT = 'HH:mm';

export const formatToDate = (date: Date | number) => format(date, DATE_FORMAT);
export const formatToTime = (date: Date | number) => format(date, TIME_FORMAT);

export const now = () => Number(new Date());

export const fullWeekdayIds = [
    'sunday',
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
];

export const shortWeekdayIds = [
    'mon',
    'tue',
    'wed',
    'thu',
    'fri',
    'sat',
    'sun',
];

export const shortMonthIds = [
    'jan',
    'feb',
    'mar',
    'apr',
    'may',
    'jun',
    'jul',
    'aug',
    'sep',
    'oct',
    'nov',
    'dec',
];
