import React, { ReactNode } from 'react';
import { IntlProvider as Provider } from 'react-intl';
import en from './en';

const i18n = {
    locale: 'en',
    defaultLocale: 'en',
    messages: en,
};

interface IntlProviderProps {
    children: ReactNode;
}

export const IntlProvider = ({ children }: IntlProviderProps) => (
    <Provider {...i18n}>{children}</Provider>
);
