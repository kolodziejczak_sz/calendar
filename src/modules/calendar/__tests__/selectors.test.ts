import { createMockState } from '@root/test/mockStore';
import { selectCalendarView, selectSelectedDateString } from '../selectors';

describe('Calendar selectors tests', () => {
    const state = createMockState();

    it('selectCalendarView', () => {
        const result = selectCalendarView(state);
        expect(result).toBe(state.calendar.view);
    });

    it('selectSelectedDateString', () => {
        const result = selectSelectedDateString(state);
        expect(result).toBe(state.calendar.selectedDateString);
    });
});
