import { ActionCreatorWithoutPayload } from '@reduxjs/toolkit';
import { createMockCalendarState } from '@root/test/mockStore';
import { addDays, addMonths, formatToDate, now } from '@root/common/utils/date';
import { actions, reducer, CalendarView } from '../slice';

describe('Calendar reducer tests', () => {
    const state = createMockCalendarState();

    it('should handle viewChanged action', () => {
        const nextState = reducer(state, actions.viewChanged(CalendarView.DAY));
        expect(nextState.view).toBe(CalendarView.DAY);
    });

    it('should reset selected date when viewChanged brings no change', () => {
        const customState = createMockCalendarState({
            view: CalendarView.DAY,
            selectedDateString: '',
        });
        const nextState = reducer(
            customState,
            actions.viewChanged(CalendarView.DAY)
        );
        expect(nextState.selectedDateString).toBe(state.selectedDateString);
    });

    it('should handle dateSelected action', () => {
        const newDate = formatToDate(now());
        const nextState = reducer(state, actions.dateSelected(newDate));
        expect(nextState.selectedDateString).toBe(newDate);
    });

    const testPeriodsAction = (actionCreator: ActionCreatorWithoutPayload) => {
        const today = new Date(now());
        const todayString = formatToDate(today);
        const amount = actionCreator === actions.nextPeriodSelected ? 1 : -1;

        describe(`${actionCreator.type} action`, () => {
            it('should change date by given context', () => {
                const statewithDay = createMockCalendarState({
                    view: CalendarView.DAY,
                    selectedDateString: todayString,
                });

                const nextStateWithDay = reducer(statewithDay, actionCreator());
                const expectedDay = formatToDate(addDays(today, amount));
                expect(nextStateWithDay.selectedDateString).toBe(expectedDay);

                const stateWithMonth = createMockCalendarState({
                    view: CalendarView.MONTH,
                    selectedDateString: todayString,
                });

                const nextStateWithMonth = reducer(
                    stateWithMonth,
                    actionCreator()
                );
                const expectedMonth = formatToDate(addMonths(today, amount));
                expect(nextStateWithMonth.selectedDateString).toBe(
                    expectedMonth
                );
            });

            it('should change date correctly', () => {
                const state = createMockCalendarState();
                const nextState = reducer(state, actionCreator());
                const comparisionFn = (a, b) => (amount === 1 ? a > b : a < b);

                expect(
                    comparisionFn(
                        new Date(nextState.selectedDateString),
                        new Date(state.selectedDateString)
                    )
                ).toBeTruthy();
            });
        });
    };

    testPeriodsAction(actions.nextPeriodSelected);
    testPeriodsAction(actions.prevPeriodSelected);
});
