import { createReducer, createAction } from '@reduxjs/toolkit';
import { formatToDate, now, addDays, addMonths } from '@root/common/utils/date';

export enum CalendarView {
    DAY,
    MONTH,
}

const viewChanged = createAction<CalendarView>('viewChanged');
const nextPeriodSelected = createAction('nextPeriodSelected');
const prevPeriodSelected = createAction('prevPeriodSelected');
const dateSelected = createAction<string>('dateSelected');

export interface CalendarState {
    view: CalendarView;
    selectedDateString: string;
}

export const initialState: CalendarState = {
    view: CalendarView.MONTH,
    selectedDateString: formatToDate(now()),
};

export const reducer = createReducer(initialState, (builder) => {
    builder
        .addCase(prevPeriodSelected, (state) => {
            state.selectedDateString = getNextDateString(state, -1);
        })
        .addCase(nextPeriodSelected, (state) => {
            state.selectedDateString = getNextDateString(state, 1);
        })
        .addCase(dateSelected, (state, { payload }) => {
            state.selectedDateString = payload;
            state.view = CalendarView.DAY;
        })
        .addCase(viewChanged, (state, { payload }) => {
            if (payload === state.view) {
                state.selectedDateString = initialState.selectedDateString;
            }
            state.view = payload;
        });

    const getNextDateString = (state: CalendarState, amount: number) => {
        const shouldChangeMonth = state.view === CalendarView.MONTH;
        const transformFn = shouldChangeMonth ? addMonths : addDays;
        const selectedDate = new Date(state.selectedDateString);

        const nextDate = transformFn(selectedDate, amount);
        return formatToDate(nextDate);
    };
});

export const actions = {
    dateSelected,
    nextPeriodSelected,
    prevPeriodSelected,
    viewChanged,
};
