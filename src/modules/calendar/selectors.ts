import { createSelector } from 'reselect';
import { AppState } from '@root/store';

const selectCalendar = (state: AppState) => state.calendar;

export const selectCalendarView = createSelector(
    selectCalendar,
    (calendar) => calendar.view
);

export const selectSelectedDateString = createSelector(
    selectCalendar,
    (calendar) => calendar.selectedDateString
);
