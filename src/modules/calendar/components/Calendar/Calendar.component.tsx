import React, { MouseEvent, ReactElement } from 'react';
import { FormattedMessage } from '@root/modules/intl';
import {
    eachDayOfInterval,
    formatToDate,
    endOfWeek,
    endOfMonth,
    isSameDay,
    isSameMonth,
    now,
    shortWeekdayIds,
    startOfWeek,
} from '@root/common/utils/date';
import './Calendar.styles.scss';

interface CalendarCellComponentProps {
    dateString: string;
    isMainMonth: boolean;
    isToday: boolean;
}

export type CalendarCellComponent = (
    props: CalendarCellComponentProps
) => ReactElement | null;

interface CalendarProps {
    month: number;
    year: number;
    CellComponent?: CalendarCellComponent;
    onSelectDate?: (dateString: string) => void;
}

const DefaultCalendarCellComponent = ({
    dateString,
}: CalendarCellComponentProps) => <>{dateString}</>;

export const Calendar = ({
    month,
    year,
    CellComponent = DefaultCalendarCellComponent,
    onSelectDate,
}: CalendarProps) => {
    const currentDate = now();
    const monthStart = new Date(year, month);
    const monthEnd = endOfMonth(monthStart);
    const start = startOfWeek(monthStart);
    const end = endOfWeek(monthEnd);

    const selectedDateHandler = ({
        currentTarget,
        target,
    }: MouseEvent<HTMLDivElement>) => {
        let eventBroker = target as HTMLElement;
        while (eventBroker !== currentTarget) {
            const selectedDate = eventBroker.dataset.date;
            if (selectedDate) {
                onSelectDate?.(selectedDate);
                return;
            }
            eventBroker = eventBroker.parentElement;
        }
    };

    const weekdays = shortWeekdayIds.map((weekdayId) => (
        <div className="Calendar__headerCell" key={weekdayId}>
            <FormattedMessage id={weekdayId} />
        </div>
    ));

    const cells = eachDayOfInterval({ start, end }).map((date) => {
        const dateString = formatToDate(date);
        const isMainMonth = isSameMonth(date, monthStart);
        const isToday = isSameDay(date, currentDate);

        return (
            <div
                className="Calendar__cell"
                key={dateString}
                title={dateString}
                data-date={dateString}
            >
                <CellComponent
                    dateString={dateString}
                    isMainMonth={isMainMonth}
                    isToday={isToday}
                />
            </div>
        );
    });

    return (
        <div className="Calendar">
            <div className="Calendar__grid">{weekdays}</div>
            <div
                className="Calendar__grid Calendar__cells"
                onClick={selectedDateHandler}
            >
                {cells}
            </div>
        </div>
    );
};
