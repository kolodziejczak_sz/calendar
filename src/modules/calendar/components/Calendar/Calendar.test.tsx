import React from 'react';
import { render } from '@root/test/render';
import { now, isSameDay, isSameMonth } from '@root/common/utils/date';
import { Calendar, CalendarCellComponent } from './Calendar.component';

describe('<Calendar>', () => {
    const today = new Date(now());

    const props = {
        month: today.getMonth(),
        year: today.getFullYear(),
        onSelectDate: jest.fn(),
    };

    beforeEach(() => jest.clearAllMocks());

    it('Should render correctly', () => {
        const { asFragment } = render(<Calendar {...props} />);

        expect(asFragment()).toMatchSnapshot();
    });

    it('Should handle props correctly', () => {
        const CellComponent = jest.fn(
            ({ dateString, isMainMonth, isToday }) => {
                const date = new Date(dateString);

                if (isMainMonth) expect(isSameMonth(date, today)).toBe(true);
                if (isToday) expect(isSameDay(date, today)).toBe(true);
                return null;
            }
        ) as jest.MockedFunction<CalendarCellComponent>;

        render(<Calendar {...props} CellComponent={CellComponent} />);

        expect(CellComponent).toBeCalledTimes(7 * 5);
    });
});
