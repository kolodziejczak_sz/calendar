import React from 'react';
import { render } from '@root/test/render';
import { createMockCalendarState } from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { SelectedDate } from './SelectedDate.component';

describe('<SelectedDate>', () => {
    it('Should render correctly', () => {
        const { asFragment } = render(<SelectedDate />, {
            calendar: createMockCalendarState({
                selectedDateString: formatToDate(now()),
            }),
        });

        expect(asFragment()).toMatchInlineSnapshot(`
            <DocumentFragment>
              <span
                class="SelectedDate"
              >
                monday 2
              </span>
            </DocumentFragment>
        `);
    });
});
