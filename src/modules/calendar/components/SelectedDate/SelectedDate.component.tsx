import React from 'react';
import { useSelector } from 'react-redux';
import cx from 'classnames';
import { fullWeekdayIds } from '@root/common/utils/date';
import { selectSelectedDateString } from '@root/modules/calendar';
import { FormattedMessage } from '@root/modules/intl';
import './SelectedDate.styles.scss';

interface SelectedDateProps {
    className?: string;
}

export const SelectedDate = ({ className }: SelectedDateProps) => {
    const selectedDateString = useSelector(selectSelectedDateString);
    const selectedDate = new Date(selectedDateString);

    const weekdayIndex = selectedDate.getDay();
    const weekdayTranslateId = fullWeekdayIds[weekdayIndex];

    return (
        <span className={cx('SelectedDate', className)}>
            <FormattedMessage id={weekdayTranslateId} />{' '}
            {selectedDate.getDate()}
        </span>
    );
};
