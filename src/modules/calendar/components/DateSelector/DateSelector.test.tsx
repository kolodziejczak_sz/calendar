import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { render } from '@root/test/render';
import { createMockCalendarState } from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { actions } from '../../slice';
import { DateSelector } from './DateSelector.component';

describe('<DateSelector>', () => {
    it('Should connect with store', async () => {
        const { getDispatchedActions, clearDispatchedActions } = render(
            <DateSelector />,
            {
                calendar: createMockCalendarState({
                    selectedDateString: formatToDate(now()),
                }),
            }
        );

        const prevPeriodEl = await screen.findByTestId('prevPeriod');
        userEvent.click(prevPeriodEl);
        expect(getDispatchedActions()).toEqual([actions.prevPeriodSelected()]);

        clearDispatchedActions();

        const nextPeriodEl = await screen.findByTestId('nextPeriod');
        userEvent.click(nextPeriodEl);
        expect(getDispatchedActions()).toEqual([actions.nextPeriodSelected()]);
    });
});
