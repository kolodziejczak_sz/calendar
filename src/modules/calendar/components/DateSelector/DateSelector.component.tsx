import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cx from 'classnames';
import { IconButton } from '@root/common/components';
import { selectCalendarView } from '../../selectors';
import { actions, CalendarView } from '../../slice';
import { SelectedMonth } from '../SelectedMonth';
import './DateSelector.styles.scss';

interface DateSelectorProps {
    className?: string;
}

export const DateSelector = ({ className }: DateSelectorProps) => {
    const dispatch = useDispatch();
    const calendarView = useSelector(selectCalendarView);
    const isMonthView = calendarView === CalendarView.MONTH;
    const labels = isMonthView
        ? ['prevMonth', 'nextMonth']
        : ['prevDay', 'nextDay'];

    return (
        <div className={cx('DateSelector', className)}>
            <SelectedMonth />
            <IconButton
                name="navigateBefore"
                title={labels[0]}
                data-testid="prevPeriod"
                onClick={() => dispatch(actions.prevPeriodSelected())}
            />
            <IconButton
                name="navigateNext"
                data-testid="nextPeriod"
                title={labels[1]}
                onClick={() => dispatch(actions.nextPeriodSelected())}
            />
        </div>
    );
};
