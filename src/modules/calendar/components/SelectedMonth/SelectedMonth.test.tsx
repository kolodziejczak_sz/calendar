import React from 'react';
import { render } from '@root/test/render';
import { createMockCalendarState } from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { SelectedMonth } from './SelectedMonth.component';

describe('<SelectedMonth>', () => {
    it('Should render correctly', () => {
        const { asFragment } = render(<SelectedMonth />, {
            calendar: createMockCalendarState({
                selectedDateString: formatToDate(now()),
            }),
        });

        expect(asFragment()).toMatchInlineSnapshot(`
            <DocumentFragment>
              <span
                class="SelectedMonth"
              >
                mar 2020
              </span>
            </DocumentFragment>
        `);
    });
});
