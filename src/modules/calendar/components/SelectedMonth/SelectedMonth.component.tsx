import React from 'react';
import { useSelector } from 'react-redux';
import cx from 'classnames';
import { shortMonthIds } from '@root/common/utils/date';
import { selectSelectedDateString } from '@root/modules/calendar';
import { FormattedMessage } from '@root/modules/intl';
import './SelectedMonth.styles.scss';

interface SelectedMonthProps {
    className?: string;
}

export const SelectedMonth = ({ className }: SelectedMonthProps) => {
    const selectedDateString = useSelector(selectSelectedDateString);
    const selectedDate = new Date(selectedDateString);

    const monthIndex = selectedDate.getMonth();
    const monthTranslateId = shortMonthIds[monthIndex];

    return (
        <span className={cx('SelectedMonth', className)}>
            <FormattedMessage id={monthTranslateId} />{' '}
            {selectedDate.getFullYear()}
        </span>
    );
};
