import { createMockState, createMockEventsState } from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { noEventId } from '../slice';
import {
    selectEventById,
    selectEventCountForDate,
    selectEventsForDate,
    selectSelectedEventId,
} from '../selectors';

describe('Events selectors tests', () => {
    const event = { id: '1', name: 'test', startTimestamp: 0, endTimestamp: 0 };
    const dateTimestamp = now();

    it('selectEventById', () => {
        const state = createMockState({
            events: createMockEventsState(noEventId, [event]),
        });
        const result = selectEventById(event.id)(state);

        expect(result).toEqual(event);
    });

    it('selectEventCountForDate', () => {
        const dateString = formatToDate(dateTimestamp);
        const state = createMockState({
            events: createMockEventsState(noEventId, [
                { startTimestamp: dateTimestamp },
            ]),
        });

        const result = selectEventCountForDate(dateString)(state);
        expect(result).toBe(1);
    });

    it('selectEventsForDate', () => {
        const dateString = formatToDate(dateTimestamp);
        const fooEvent = { name: 'foo', startTimestamp: dateTimestamp };
        const state = createMockState({
            events: createMockEventsState(noEventId, [fooEvent]),
        });

        const result = selectEventsForDate(dateString)(state);

        expect(result.length).toBe(1);
        expect(
            result.find((event) => event.name === fooEvent.name)
        ).toBeTruthy();
    });

    it('selectSelectedEventId', () => {
        const state = createMockState({
            events: createMockEventsState(event.id, []),
        });
        const result = selectSelectedEventId(state);

        expect(result).toEqual(event.id);
    });
});
