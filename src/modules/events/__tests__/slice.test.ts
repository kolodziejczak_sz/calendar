import { createMockEventsState } from '@root/test/mockStore';
import { actions as calendarActions } from '@root/modules/calendar';
import { actions, reducer, noEventId } from '../slice';

describe('Events reducer tests', () => {
    const event = { id: '1', name: 'test', startTimestamp: 0, endTimestamp: 0 };

    it('should handle eventSelected action', () => {
        const state = createMockEventsState(noEventId, []);

        const nextState = reducer(state, actions.eventSelected(event.id));

        expect(nextState.selectedId).toBe(event.id);
    });

    it('should handle eventRemoved action', () => {
        const state = createMockEventsState(event.id, [event]);

        const nextState = reducer(state, actions.eventRemoved(event.id));

        expect(nextState.selectedId).toBe(noEventId);
        expect(nextState.ids.includes(event.id)).toBeFalsy();
    });

    it('should reset selectedId on any calendar action', () => {
        const state = createMockEventsState(event.id, []);

        Object.values(calendarActions).forEach((actionCreator) => {
            state.selectedId = event.id;
            const calendarAction = actionCreator();
            const nextState = reducer(state, calendarAction);

            expect(nextState.selectedId).toBe(noEventId);
        });
    });

    describe('eventSaved action', () => {
        it('should create a new event if event.id is null', () => {
            const state = createMockEventsState(noEventId, []);
            const newEvent = { ...event, id: null, name: 'any' };

            const nextState = reducer(state, actions.eventSaved(newEvent));

            expect(nextState.ids.length).toBe(1);
        });

        it('should update an exisiting event if event id is defined', () => {
            const state = createMockEventsState(noEventId, [event]);
            const update = { ...event, name: 'xxx' };

            const nextState = reducer(state, actions.eventSaved(update));

            expect(nextState.ids.includes(update.id)).toBeTruthy();
            expect(nextState.entities[update.id]).toEqual(update);
            expect(nextState.ids.length).toBe(1);
        });
    });
});
