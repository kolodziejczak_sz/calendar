import React from 'react';
import { useSelector } from 'react-redux';
import cx from 'classnames';
import { formatToTime } from '@root/common/utils/date';
import { selectEventById } from '../../selectors';
import './Event.styles.scss';

interface EventProps {
    id: string;
    onClick?: (eventId: string) => void;
    className?: string;
}

export const Event = ({ id, className, onClick }: EventProps) => {
    const event = useSelector(selectEventById(id));

    if (!event) {
        return null;
    }

    const dateStartString = formatToTime(new Date(event.startTimestamp));
    const dateEndString = formatToTime(new Date(event.endTimestamp));

    return (
        <button
            className={cx('Event', className)}
            onClick={() => onClick?.(id)}
        >
            <span className="Event__time">
                <span>{dateStartString}</span>
                <span>{dateEndString}</span>
            </span>
            <span className="Event__name">{event.name}</span>
        </button>
    );
};
