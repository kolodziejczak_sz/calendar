import React from 'react';
import { render } from '@root/test/render';
import { createMockEventsState } from '@root/test/mockStore';
import { Event } from './Event.component';
import { noEventId } from '../../slice';

describe('<Event>', () => {
    it('Should render correctly', () => {
        const event = {
            name: 'foo',
            id: '1',
            startTimestamp: 0,
            endTimestamp: 0,
        };

        const { asFragment } = render(<Event id={event.id} />, {
            events: createMockEventsState(noEventId, [event]),
        });

        expect(asFragment()).toMatchInlineSnapshot(`
            <DocumentFragment>
              <button
                class="Event"
              >
                <span
                  class="Event__time"
                >
                  <span>
                    01:00
                  </span>
                  <span>
                    01:00
                  </span>
                </span>
                <span
                  class="Event__name"
                >
                  foo
                </span>
              </button>
            </DocumentFragment>
        `);
    });
});
