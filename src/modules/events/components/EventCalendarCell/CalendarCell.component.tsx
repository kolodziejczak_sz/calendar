import React from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';
import { selectEventCountForDate } from '../../selectors';
import './CalendarCell.styles.scss';

interface CalendarCellComponentProps {
    dateString: string;
    isMainMonth: boolean;
    isToday: boolean;
}

export const CalendarCell = ({
    dateString,
    isMainMonth,
    isToday,
}: CalendarCellComponentProps) => {
    const date = new Date(dateString).getDate();
    const eventCounter = useSelector(selectEventCountForDate(dateString));
    const shouldRenderBadge = Boolean(eventCounter);

    return (
        <button
            className={cx('CalendarCell', {
                'CalendarCell--outer': !isMainMonth,
                'CalendarCell--today': isToday,
            })}
        >
            <span className="CalendarCell__date">
                {date}
                {shouldRenderBadge && (
                    <span
                        className="CalendarCell__badge"
                        data-testid="day-events-badge"
                    >
                        {eventCounter}
                    </span>
                )}
            </span>
        </button>
    );
};
