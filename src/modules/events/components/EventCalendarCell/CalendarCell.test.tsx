import React from 'react';
import { render } from '@root/test/render';
import { formatToDate, now } from '@root/common/utils/date';
import { CalendarCell } from './CalendarCell.component';

describe('<CalendarCell>', () => {
    it('Should render correctly', () => {
        const date = new Date(now());
        const { asFragment } = render(
            <CalendarCell
                isToday={true}
                isMainMonth={true}
                dateString={formatToDate(date)}
            />
        );

        expect(asFragment()).toMatchInlineSnapshot(`
            <DocumentFragment>
              <button
                class="CalendarCell CalendarCell--today"
              >
                <span
                  class="CalendarCell__date"
                >
                  2
                </span>
              </button>
            </DocumentFragment>
        `);
    });
});
