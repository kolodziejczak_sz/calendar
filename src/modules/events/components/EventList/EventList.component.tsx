import React, { useCallback } from 'react';
import cx from 'classnames';
import { useSelector, useDispatch } from 'react-redux';
import { selectSelectedDateString } from '@root/modules/calendar';
import { FormattedMessage } from '@root/modules/intl';
import { Event } from '../Event';
import { selectEventsForDate } from '../../selectors';
import { actions } from '../../slice';
import './EventList.styles.scss';

interface EventListProps {
    className?: string;
}

export const EventList = ({ className }: EventListProps) => {
    const dispatch = useDispatch();
    const selectedDateString = useSelector(selectSelectedDateString);
    const events = useSelector(selectEventsForDate(selectedDateString));
    const clickHandler = useCallback((id: string) => {
        dispatch(actions.eventSelected(id));
    }, []);

    return (
        <div className={cx('EventList', className)} data-testid="eventList">
            {events.length ? (
                events.map(({ id: eventId }) => (
                    <Event
                        key={eventId}
                        id={eventId}
                        onClick={clickHandler}
                        className="EventList__item"
                    />
                ))
            ) : (
                <div className="EventList__empty">
                    <FormattedMessage id="noEvents" />
                </div>
            )}
        </div>
    );
};
