import React from 'react';
import { render } from '@root/test/render';
import {
    createMockCalendarState,
    createMockEventsState,
} from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { EventList } from './EventList.component';
import { noEventId } from '../../slice';

describe('<EventList>', () => {
    it('Should render correctly', () => {
        const dateTimestamp = now();
        const { asFragment } = render(<EventList />, {
            calendar: createMockCalendarState({
                selectedDateString: formatToDate(dateTimestamp),
            }),
            events: createMockEventsState(noEventId, [
                {
                    name: 'foo',
                    startTimestamp: dateTimestamp,
                    endTimestamp: dateTimestamp,
                },
                {
                    name: 'bar',
                    startTimestamp: dateTimestamp,
                    endTimestamp: dateTimestamp,
                },
            ]),
        });

        expect(asFragment()).toMatchInlineSnapshot(`
            <DocumentFragment>
              <div
                class="EventList"
                data-testid="eventList"
              >
                <button
                  class="Event EventList__item"
                >
                  <span
                    class="Event__time"
                  >
                    <span>
                      12:00
                    </span>
                    <span>
                      12:00
                    </span>
                  </span>
                  <span
                    class="Event__name"
                  >
                    foo
                  </span>
                </button>
                <button
                  class="Event EventList__item"
                >
                  <span
                    class="Event__time"
                  >
                    <span>
                      12:00
                    </span>
                    <span>
                      12:00
                    </span>
                  </span>
                  <span
                    class="Event__name"
                  >
                    bar
                  </span>
                </button>
              </div>
            </DocumentFragment>
        `);
    });
});
