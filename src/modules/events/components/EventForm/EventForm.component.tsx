import React, { ChangeEvent, FormEvent, useState } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import { AppState } from '@root/store';
import { FormattedMessage } from '@root/modules/intl';
import { FormControl, Icon, IconButton } from '@root/common/components';
import {
    addMinutes,
    formatToTime,
    formatToDate,
    now,
    set,
    roundToNearestMinutes,
} from '@root/common/utils/date';
import { selectSelectedDateString } from '@root/modules/calendar';
import {
    actions as eventActions,
    EventModel,
    newEventId,
    SelectedEventId,
} from '../../slice';
import { selectEventById, selectSelectedEventId } from '../../selectors';
import './EventForm.styles.scss';

interface EventFormComponentProps {
    className?: string;
    eventId: SelectedEventId;
    eventName: EventModel['name'];
    eventStartTimestamp: EventModel['startTimestamp'];
    eventEndTimetamp: EventModel['startTimestamp'];
    eventSaved: (event: EventModel) => void;
    eventSelected: (id: SelectedEventId) => void;
    eventRemoved: (id: EventModel['id']) => void;
}

const EventFormComponent = ({
    className,
    eventId,
    eventName,
    eventStartTimestamp,
    eventEndTimetamp,
    eventSaved,
    eventSelected,
    eventRemoved,
}: EventFormComponentProps) => {
    const [name, setName] = useState(eventName);
    const [startTime, setStartTime] = useState(() =>
        formatToTime(eventStartTimestamp)
    );
    const [endTime, setEndTime] = useState(() =>
        formatToTime(eventEndTimetamp)
    );

    const changeHandler = (setter: (value: string) => void) => ({
        currentTarget,
    }: ChangeEvent<HTMLInputElement>) => {
        const nextValue = currentTarget.value;
        setter(nextValue);
    };

    const submitHandler = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const isFormValid = event.currentTarget.checkValidity();
        if (!isFormValid) {
            return;
        }

        const toTimeStamp = (timeValue: string) => {
            const [hoursPart, minutesPart] = timeValue.split(':');
            return Number(
                set(eventStartTimestamp, {
                    hours: Number(hoursPart),
                    minutes: Number(minutesPart),
                })
            );
        };
        const newEvent: EventModel = {
            id: eventId,
            name,
            startTimestamp: toTimeStamp(startTime),
            endTimestamp: toTimeStamp(endTime),
        };

        eventSaved(newEvent);
    };

    const isEditMode = Boolean(eventId);

    return (
        <form
            onSubmit={submitHandler}
            className={cx('EventForm', className)}
            data-testid="eventForm"
        >
            <header className="EventForm__header">
                <h2 className="EventForm__title">
                    <FormattedMessage
                        id={isEditMode ? 'eventEdit' : 'eventNew'}
                    />
                </h2>
                <IconButton title="eventSave" name="save" type="submit" />
                <IconButton
                    title="eventRemove"
                    name="remove"
                    onClick={() => eventRemoved(eventId)}
                    disabled={!isEditMode}
                />
                <IconButton
                    title="cancel"
                    name="cancel"
                    onClick={() => eventSelected(undefined)}
                />
            </header>
            <div className="EventForm__row">
                <FormControl
                    name="name"
                    type="text"
                    title="eventName"
                    onChange={changeHandler(setName)}
                    value={name}
                    required
                />
            </div>
            <div className="EventForm__row">
                <Icon name="calendarMonth" />
                <span>{formatToDate(eventStartTimestamp)}</span>
            </div>
            <div className="EventForm__row">
                <Icon name="clock" />
                <FormControl
                    name="startTime"
                    title="eventStartTime"
                    type="time"
                    onChange={changeHandler(setStartTime)}
                    max={endTime}
                    value={startTime}
                    required
                />
                <span>
                    <FormattedMessage id="timeSeparator" />
                </span>
                <FormControl
                    name="endTime"
                    title="eventEndTime"
                    type="time"
                    onChange={changeHandler(setEndTime)}
                    min={startTime}
                    value={endTime}
                    required
                />
            </div>
        </form>
    );
};

const getDefaultTimestamps = (date: Date) => {
    const hhmmStart = roundToNearestMinutes(now(), { nearestTo: 30 });
    const startDate = set(date, {
        hours: hhmmStart.getHours(),
        minutes: hhmmStart.getMinutes(),
    });

    const startTimestamp = Number(startDate);
    const endTimestamp = Number(addMinutes(startTimestamp, 30));

    return { startTimestamp, endTimestamp };
};

const mapStateToProps = (state: AppState) => {
    const selectedDateString = selectSelectedDateString(state);
    const selectedEventId = selectSelectedEventId(state);
    const defaultTimestamps = getDefaultTimestamps(
        new Date(selectedDateString)
    );
    const {
        name = '',
        startTimestamp = defaultTimestamps.startTimestamp,
        endTimestamp = defaultTimestamps.endTimestamp,
    } = selectEventById(selectedEventId)(state) || {};

    return {
        eventId: selectedEventId || newEventId,
        eventName: name,
        eventStartTimestamp: startTimestamp,
        eventEndTimetamp: endTimestamp,
    };
};

export const EventForm = connect(
    mapStateToProps,
    eventActions
)(EventFormComponent);
