import React from 'react';
import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/react';
import { render } from '@root/test/render';
import {
    createMockCalendarState,
    createMockEventsState,
} from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { EventForm } from './EventForm.component';
import { actions as eventActions, noEventId } from '../../slice';

describe('<EventForm>', () => {
    const dateTimestamp = now();
    const event = {
        id: '1',
        name: 'foo',
        startTimestamp: dateTimestamp,
        endTimestamp: dateTimestamp,
    };
    const state = {
        calendar: createMockCalendarState({
            selectedDateString: formatToDate(dateTimestamp),
        }),
        events: createMockEventsState(noEventId, [event]),
    };

    it('Should handle close button', async () => {
        const { getDispatchedActions } = render(<EventForm />, state);

        const cancelEl = await screen.getByTitle('cancel');
        userEvent.click(cancelEl);

        expect(getDispatchedActions()).toEqual([
            eventActions.eventSelected(noEventId),
        ]);
    });

    it('Should handle remove button', async () => {
        const eventId = '1';
        const stateWithSelectedEvent = {
            ...state,
            events: { ...state.events, selectedId: eventId },
        };
        const { getDispatchedActions } = render(
            <EventForm />,
            stateWithSelectedEvent
        );

        const removeEl = await screen.getByTitle('eventRemove');
        userEvent.click(removeEl);

        expect(getDispatchedActions()).toEqual([
            eventActions.eventRemoved(eventId),
        ]);
    });

    it('Should handle validation', async () => {
        const { getDispatchedActions } = render(<EventForm />, state);

        const startTimeEl = await screen.getByTitle('eventStartTime');
        const endTimeEl = await screen.getByTitle('eventEndTime');
        const saveEl = await screen.getByTitle('eventSave');

        userEvent.type(startTimeEl, '10:30');
        userEvent.type(endTimeEl, '10:00');
        userEvent.click(saveEl);

        expect(getDispatchedActions()).toEqual([]);
    });

    it('Should handle a new event creation', async () => {
        const { getDispatchedActions } = render(<EventForm />, state);
        const saveEl = await screen.getByTitle('eventSave');
        const eventNameEl = await screen.getByTitle('eventName');

        userEvent.type(eventNameEl, 'foo');
        userEvent.click(saveEl);

        expect(getDispatchedActions()).toMatchInlineSnapshot(`
            Array [
              Object {
                "payload": Object {
                  "endTimestamp": 1583148600000,
                  "id": null,
                  "name": "foo",
                  "startTimestamp": 1583146800000,
                },
                "type": "eventSaved",
              },
            ]
        `);
    });

    it('Should handle an event update', async () => {
        const stateForUpdate = {
            ...state,
            events: { ...state.events, selectedId: event.id },
        };
        const { getDispatchedActions } = render(<EventForm />, stateForUpdate);

        const saveEl = await screen.getByTitle('eventSave');
        const eventNameEl = await screen.getByTitle('eventName');
        const newName = '1234';
        userEvent.clear(eventNameEl);
        userEvent.type(eventNameEl, newName);
        userEvent.click(saveEl);

        expect(getDispatchedActions()).toEqual([
            eventActions.eventSaved({ ...event, name: newName }),
        ]);
    });
});
