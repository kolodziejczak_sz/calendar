import React from 'react';
import { screen, findByTestId } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { render } from '@root/test/render';
import { addDays, formatToDate, now } from '@root/common/utils/date';
import { CalendarView } from '@root/modules/calendar';
import { EventCalendar } from './EventCalendar.component';

describe('<EventCalendar>', () => {
    const today = new Date(now());
    const todayString = formatToDate(today);

    it('Should connect with store', async () => {
        const eventId = '1';
        const initialState = {
            calendar: {
                view: CalendarView.MONTH,
                selectedDateString: formatToDate(addDays(today, 1)),
            },
            events: {
                selectedId: eventId,
                ids: [eventId],
                entities: {
                    [eventId]: {
                        id: eventId,
                        startTimestamp: Number(today),
                        endTimestamp: Number(today) + 60_000,
                        name: 'any',
                    },
                },
            },
        };

        const { getDispatchedActions } = render(
            <EventCalendar />,
            initialState
        );
        const todayCell = await screen.findByTitle(todayString);
        const badge = await findByTestId(todayCell, 'day-events-badge');

        expect(badge).toHaveTextContent('1');

        userEvent.click(todayCell);

        const dispatchedActions = getDispatchedActions();
        expect(dispatchedActions).toMatchInlineSnapshot(`
            Array [
              Object {
                "payload": "2020-03-02",
                "type": "dateSelected",
              },
            ]
        `);
    });
});
