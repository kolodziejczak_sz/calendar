import { connect } from 'react-redux';
import { AppState } from '@root/store';
import {
    selectSelectedDateString,
    actions,
    Calendar,
} from '@root/modules/calendar';
import { CalendarCell } from '../EventCalendarCell';

const mapStateToProps = (state: AppState) => {
    const selectedDateString = selectSelectedDateString(state);
    const date = new Date(selectedDateString);

    return {
        CellComponent: CalendarCell,
        month: date.getMonth(),
        year: date.getFullYear(),
    };
};

const mapDispatchToProps = {
    onSelectDate: actions.dateSelected,
};

export const EventCalendar = connect(
    mapStateToProps,
    mapDispatchToProps
)(Calendar);
