import { nanoid } from 'nanoid';
import {
    createEntityAdapter,
    createReducer,
    createAction,
} from '@reduxjs/toolkit';
import { actions as calendarActions } from '@root/modules/calendar';

export const newEventId = null;
export const noEventId = undefined;

export type SelectedEventId = string | typeof newEventId | typeof noEventId;

export type EventModel = {
    id: string;
    name: string;
    startTimestamp: number;
    endTimestamp: number;
};

export const eventsAdapter = createEntityAdapter<EventModel>({
    sortComparer: (a, b) => a.startTimestamp - b.endTimestamp,
});

const eventSaved = createAction<EventModel>('eventSaved');
const eventRemoved = createAction<EventModel['id']>('eventRemoved');
const eventSelected = createAction<SelectedEventId>('eventSelected');

const calendarActionCreators = Object.values(calendarActions);

export const reducer = createReducer(
    eventsAdapter.getInitialState({
        selectedId: noEventId,
    }),
    (builder) => {
        builder
            .addCase(eventSelected, (state, { payload }) => {
                state.selectedId = payload;
            })
            .addCase(eventRemoved, (state, { payload }) => {
                state.selectedId = noEventId;
                eventsAdapter.removeOne(state, payload);
            })
            .addCase(eventSaved, (state, { payload }) => {
                state.selectedId = noEventId;

                if (payload.id) {
                    eventsAdapter.updateOne(state, {
                        id: payload.id,
                        changes: payload,
                    });
                } else {
                    const newEventId = nanoid();
                    eventsAdapter.addOne(state, {
                        ...payload,
                        id: newEventId,
                    });
                }
            })
            .addMatcher(
                (action) =>
                    calendarActionCreators.some(
                        (calendarAction) => calendarAction.type === action.type
                    ),
                (state) => {
                    state.selectedId = noEventId;
                }
            );
    }
);

export const actions = {
    eventSelected,
    eventSaved,
    eventRemoved,
};
