import { createSelector } from 'reselect';
import { isSameDay } from '@root/common/utils/date';
import { AppState } from '@root/store';
import { eventsAdapter } from './slice';

const selectEvents = (state: AppState) => state.events;

const { selectAll, selectById } = eventsAdapter.getSelectors<AppState>(
    selectEvents
);

export const selectSelectedEventId = createSelector(
    selectEvents,
    (events) => events.selectedId
);

export const selectEventById = (id: string) => (state: AppState) =>
    selectById(state, id);

export const selectEventsForDate = (dateString: string) => {
    const dateTimestamp = Number(new Date(dateString));

    return createSelector(selectAll, (events) =>
        events.filter(({ startTimestamp }) =>
            isSameDay(startTimestamp, dateTimestamp)
        )
    );
};

export const selectEventCountForDate = (dateString: string) =>
    createSelector(
        selectEventsForDate(dateString),
        (events) => events.length || 0
    );
