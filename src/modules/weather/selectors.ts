import { createSelector } from 'reselect';
import { AppState } from '@root/store';

const selectWeather = (state: AppState) => state.weather;

export const selectWeatherInfoForDate = (dateString: string) =>
    createSelector(selectWeather, (weather) => weather.entities[dateString]);
