import { createEntityAdapter, createReducer } from '@reduxjs/toolkit';
import { actions } from './actions';
import { WeatherData } from './service';

export interface Weather extends Partial<WeatherData> {
    id: string;
    state: 'pending' | 'fulfilled' | 'rejected';
}

export const weatherAdapter = createEntityAdapter<Weather>();

export const reducer = createReducer(
    weatherAdapter.getInitialState(),
    (builder) => {
        builder
            .addCase(actions.weatherRequested.pending, (state, { meta }) => {
                weatherAdapter.addOne(state, {
                    id: meta.arg,
                    state: meta.requestStatus,
                });
            })
            .addCase(actions.weatherRequested.rejected, (state, { meta }) => {
                weatherAdapter.updateOne(state, {
                    id: meta.arg,
                    changes: {
                        state: meta.requestStatus,
                    },
                });
            })
            .addCase(
                actions.weatherRequested.fulfilled,
                (state, { payload, meta }) => {
                    weatherAdapter.updateOne(state, {
                        id: meta.arg,
                        changes: {
                            ...payload,
                            state: meta.requestStatus,
                        },
                    });
                }
            );
    }
);
