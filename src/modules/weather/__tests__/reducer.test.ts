import { createMockWeatherState } from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { reducer } from '../reducer';
import { actions } from '../actions';

const { weatherRequested } = actions;

describe('Weather reducer tests', () => {
    const requestId = '1';
    const date = new Date(now());
    const dateString = formatToDate(date);
    const mockWeatherData = {
        temperature: 123,
        iconName: 'foo',
    };
    const mockedWeather = {
        ...mockWeatherData,
        id: dateString,
        state: 'fulfilled' as const,
    };

    it('should handle weatherRequested.pending action', () => {
        const state = createMockWeatherState();
        const nextState = reducer(
            state,
            weatherRequested.pending(undefined, dateString)
        );

        expect(nextState.ids).toEqual([dateString]);
        expect(nextState.entities[dateString]).toEqual({
            id: dateString,
            state: 'pending',
        });
    });

    it('should handle weatherRequested.rejected action', () => {
        const state = createMockWeatherState();
        const pendingState = reducer(
            state,
            weatherRequested.pending(undefined, dateString)
        );

        const nextState = reducer(
            pendingState,
            weatherRequested.rejected(new Error('any'), requestId, dateString)
        );

        expect(nextState.ids).toEqual([dateString]);
        expect(nextState.entities[dateString]).toEqual({
            id: dateString,
            state: 'rejected',
        });
    });

    it('should handle weatherRequested.fullfiled action', () => {
        const state = createMockWeatherState();
        const pendingState = reducer(
            state,
            weatherRequested.pending(undefined, dateString)
        );

        const nextState = reducer(
            pendingState,
            weatherRequested.fulfilled(mockWeatherData, requestId, dateString)
        );

        expect(nextState.ids).toEqual([dateString]);
        expect(nextState.entities[dateString]).toEqual(mockedWeather);
    });
});
