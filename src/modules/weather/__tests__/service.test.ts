import { addMonths, formatToDate, now } from '@root/common/utils/date';
import { weatherService, WeatherData } from '../service';

describe('WeatherService tests', () => {
    describe('getWeatherIconUrl', () => {
        it('should return default icon url', () => {
            const iconUrl = weatherService.getWeatherIconUrl();
            expect(iconUrl).toMatchInlineSnapshot(
                `"http://openweathermap.org/img/wn/02d@2x.png"`
            );
        });

        it('should return icon url', () => {
            const iconUrl = weatherService.getWeatherIconUrl('anyIcon');
            expect(iconUrl).toMatchInlineSnapshot(
                `"http://openweathermap.org/img/wn/anyIcon@2x.png"`
            );
        });
    });

    describe('fetchWeatherForDate', () => {
        beforeAll(() => jest.spyOn(window, 'fetch'));

        const date = new Date(now());
        const dateString = formatToDate(date);
        const mockWeatherData: WeatherData = {
            temperature: 123,
            iconName: 'foo',
        };

        it("should throw error if requested date is not today's", async () => {
            const otherDate = addMonths(date, 1);
            const otherDateString = formatToDate(otherDate);
            try {
                await weatherService.fetchWeatherForDate(otherDateString);
            } catch (error) {
                expect(error).toMatchInlineSnapshot(
                    `"Weather data is available only for the current date"`
                );
            }
        });

        it('should throw error if response is not ok', async () => {
            (window.fetch as jest.Mock).mockResolvedValueOnce({
                ok: false,
                status: 404,
                statusText: 'not found',
                json: async () => ({
                    success: true,
                }),
            });

            try {
                await weatherService.fetchWeatherForDate(dateString);
            } catch (error) {
                expect(error).toMatchInlineSnapshot(`"404 not found"`);
            }
        });

        it('should map response to WeatherData', async () => {
            (window.fetch as jest.Mock).mockResolvedValueOnce({
                ok: true,
                json: async () => ({
                    main: { temp: mockWeatherData.temperature },
                    weather: [
                        {
                            icon: mockWeatherData.iconName,
                        },
                    ],
                }),
            });

            const result = await weatherService.fetchWeatherForDate(dateString);
            expect(result).toEqual(mockWeatherData);
        });
    });
});
