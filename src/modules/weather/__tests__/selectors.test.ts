import { createMockState, createMockWeatherState } from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { selectWeatherInfoForDate } from '../selectors';

describe('Weather selectors tests', () => {
    it('selectWeatherInfoForDate', () => {
        const date = new Date(now());
        const dateString = formatToDate(date);
        const mockedWeather = {
            id: dateString,
            temperature: 123,
            iconName: 'foo',
            state: 'fulfilled' as const,
        };
        const state = createMockState({
            weather: createMockWeatherState([mockedWeather]),
        });
        const result = selectWeatherInfoForDate(dateString)(state);

        expect(result).toEqual(mockedWeather);
    });
});
