import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cx from 'classnames';
import { selectSelectedDateString } from '@root/modules/calendar';
import { useIntl } from '@root/modules/intl';
import { selectWeatherInfoForDate } from '../../selectors';
import { weatherService } from '../../service';
import { actions as weatherActions } from '../../actions';
import './DayWeather.styles.scss';

interface DayWeatherProps {
    className?: string;
}

export const DayWeather = ({ className }: DayWeatherProps) => {
    const intl = useIntl();
    const dispatch = useDispatch();

    const selectedDateString = useSelector(selectSelectedDateString);
    const weatherInfo = useSelector(
        selectWeatherInfoForDate(selectedDateString)
    );

    if (!weatherInfo) {
        dispatch(weatherActions.weatherRequested(selectedDateString));
        return null;
    }

    const { state, iconName, temperature } = weatherInfo;
    const title = intl.formatMessage({ id: 'weatherData' });
    const text = temperature
        ? `${temperature}°C`
        : intl.formatMessage({ id: 'weatherNoData' });

    return (
        <div
            title={title}
            className={cx('DayWeather', className, {
                'DayWeather--pending': state === 'pending',
                'DayWeather--rejected': state === 'rejected',
            })}
        >
            <span className="DayWeather__text">{text}</span>
            <img src={weatherService.getWeatherIconUrl(iconName)} alt={title} />
        </div>
    );
};
