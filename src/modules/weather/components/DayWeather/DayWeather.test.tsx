import React from 'react';
import { render } from '@root/test/render';
import {
    createMockCalendarState,
    createMockWeatherState,
} from '@root/test/mockStore';
import { formatToDate, now } from '@root/common/utils/date';
import { actions } from '../../actions';
import { Weather } from '../../reducer';
import { DayWeather } from './DayWeather.component';

jest.mock('../../actions');

describe('<DayWeather>', () => {
    const dateString = formatToDate(now());

    const withWeather = (weather?: Partial<Weather>) => {
        return {
            calendar: createMockCalendarState({
                selectedDateString: dateString,
            }),
            weather: createMockWeatherState(weather ? [weather] : undefined),
        };
    };

    it('Should request weatherData when it is missing', async () => {
        const action = (actions.weatherRequested as unknown) as jest.Mock;
        action.mockImplementation(jest.fn(() => () => null));

        render(<DayWeather />, withWeather());

        expect(actions.weatherRequested).toBeCalledTimes(1);
    });

    it('Should render correctly', () => {
        const empty = render(<DayWeather />, withWeather());
        const pending = render(
            <DayWeather />,
            withWeather({ state: 'pending' })
        );
        const fulfilled = render(
            <DayWeather />,
            withWeather({ temperature: 20, state: 'fulfilled' })
        );
        const rejected = render(
            <DayWeather />,
            withWeather({ state: 'rejected' })
        );

        [empty, pending, fulfilled, rejected].forEach(({ asFragment }) => {
            expect(asFragment()).toMatchSnapshot();
        });
    });
});
