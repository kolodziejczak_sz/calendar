import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkAPI } from '@root/store';
import { selectWeatherInfoForDate } from './selectors';
import { WeatherData } from './service';

const weatherRequested = createAsyncThunk<WeatherData, string, ThunkAPI>(
    'weatherRequested',
    (dateString, { extra }) => extra.fetchWeatherForDate(dateString),
    {
        condition: (dateString: string, { getState }) => {
            const state = getState();
            const weatherForDate = selectWeatherInfoForDate(dateString)(state);
            return !Boolean(weatherForDate);
        },
    }
);

export const actions = {
    weatherRequested,
};
