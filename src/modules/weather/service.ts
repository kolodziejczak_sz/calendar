import { formatToDate, now } from '@root/common/utils/date';
import {
    currentWeatherDataApiEndpoint,
    defaultIconName,
    iconUrlEndpoint,
} from './constants';

export interface WeatherData {
    temperature: number;
    iconName: string;
}

interface WeatherApiResponse {
    main: { temp: number };
    weather: {
        icon: string;
    }[];
}

export const weatherService = {
    getWeatherIconUrl: (iconName: string = defaultIconName) => {
        return `${iconUrlEndpoint}/${iconName}@2x.png`;
    },

    fetchWeatherForDate: async (dateString: string) => {
        const currentDateString = formatToDate(now());
        const isCurrentDateRequested = dateString === currentDateString;

        if (!isCurrentDateRequested) {
            throw 'Weather data is available only for the current date';
        }

        const response = await fetch(currentWeatherDataApiEndpoint);
        if (!response.ok) {
            throw `${response.status} ${response.statusText}`;
        }

        const jsonResponse = (await response.json()) as WeatherApiResponse;
        const weatherData: WeatherData = {
            temperature: jsonResponse.main?.temp,
            iconName:
                jsonResponse.weather?.find((weather) => weather.icon)?.icon ||
                defaultIconName,
        };

        return weatherData;
    },
};
