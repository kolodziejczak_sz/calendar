const { SNOWPACK_PUBLIC_WEATHER_API_KEY } = import.meta.env;

export const iconUrlEndpoint = 'http://openweathermap.org/img/wn';

export const defaultIconName = '02d';

export const currentWeatherDataApiEndpoint = [
    'https://api.openweathermap.org/data/2.5/weather',
    `?q=Sosnowiec&units=metric&appid=${SNOWPACK_PUBLIC_WEATHER_API_KEY}`,
].join('');
