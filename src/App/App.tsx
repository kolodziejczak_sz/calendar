import React from 'react';
import { CalendarPage } from '@root/pages/calendar';
import './App.styles.scss';

export const App = () => {
    return <CalendarPage />;
};
