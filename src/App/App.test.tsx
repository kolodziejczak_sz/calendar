import React from 'react';
import { render } from '@root/test/render';
import { App } from './App';

describe('<App>', () => {
    it('renders without error', () => {
        const { container } = render(<App />);
        expect(container.firstChild).not.toBeNull();
    });
});
