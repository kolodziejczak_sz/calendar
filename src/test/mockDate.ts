jest.mock('@root/common/utils/date', () => ({
    ...jest.requireActual('@root/common/utils/date'),
    now: () => Number(new Date(2020, 2, 2, 12)),
}));
