import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { AnyAction } from '@reduxjs/toolkit';
import { render as tlRender } from '@testing-library/react';
import { AppState } from '@root/store';
import { IntlProvider } from 'react-intl';
import { createMockStore } from './mockStore';

export const render = (
    Component: JSX.Element,
    partialState?: Partial<AppState>
) => {
    const store = createMockStore(partialState);

    const dispatchedActions = [];
    const getDispatchedActions = () => dispatchedActions;
    const clearDispatchedActions = () => (dispatchedActions.length = 0);

    const storeDispatch = store.dispatch;
    const newDispatch: any = (action: AnyAction) => {
        dispatchedActions.push(action);
        storeDispatch(action);
    };
    store.dispatch = newDispatch;

    const renderResult = tlRender(
        <StoreProvider store={store}>
            <IntlProvider locale="en" messages={{}} onError={() => null}>
                {Component}
            </IntlProvider>
        </StoreProvider>
    );

    return {
        ...renderResult,
        getDispatchedActions,
        clearDispatchedActions,
    };
};
