import { configureStore } from '@reduxjs/toolkit';
import { rootReducer, AppState } from '@root/store';
import {
    formatToDate,
    now,
    roundToNearestMinutes,
} from '@root/common/utils/date';
import { EventModel } from '@root/modules/events';
import { weatherService, Weather } from '@root/modules/weather';

const defaultState = configureStore({ reducer: rootReducer }).getState();

const mockWeatherService = {
    fetchWeatherForDate: jest.fn(),
    getWeatherIconUrl: jest.fn(),
} as typeof weatherService;

export const createMockStore = (partialState?: Partial<AppState>) =>
    configureStore({
        reducer: rootReducer,
        preloadedState: createMockState(partialState),
        middleware: (getDefaultMiddleware) =>
            getDefaultMiddleware({
                thunk: {
                    extraArgument: mockWeatherService,
                },
            }),
    });

export const createMockState = (partialState?: Partial<AppState>) => {
    return {
        events: createMockEventsState(),
        calendar: createMockCalendarState(),
        weather: createMockWeatherState(),
        ...partialState,
    };
};

export const createMockCalendarState = (
    partialState?: Partial<AppState['calendar']>
) => {
    return {
        ...defaultState.calendar,
        ...partialState,
    };
};

const firstEventTimestamp = Number(
    roundToNearestMinutes(now(), { nearestTo: 5 })
);

export const createMockEventsState = (
    selectedId?: AppState['events']['selectedId'],
    eventsDraft?: Partial<EventModel>[]
): AppState['events'] => {
    const events = eventsDraft?.map((draft, index) => ({
        id: `${index}`,
        name: `Event ${index}`,
        startTimestamp: firstEventTimestamp,
        endTimestamp: firstEventTimestamp + 60_000 * 5,
        ...draft,
    }));

    const entities =
        events?.reduce(
            (acc, event) => ({
                ...acc,
                [event.id]: event,
            }),
            {}
        ) || [];

    return {
        selectedId,
        entities,
        ids: Object.keys(entities),
    };
};

export const createMockWeatherState = (
    weatherEntitiesDraft?: Partial<Weather>[]
): AppState['weather'] => {
    const weather = weatherEntitiesDraft?.map((draft) => ({
        state: 'rejected',
        temperature: undefined,
        iconName: 'iconName',
        id: formatToDate(new Date(2020, 2, 2)),
        ...draft,
    }));

    const entities =
        weather?.reduce(
            (acc, event) => ({
                ...acc,
                [event.id]: event,
            }),
            {}
        ) || [];

    return {
        entities,
        ids: Object.keys(entities),
    };
};
