import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as StoreProvider } from 'react-redux';
import { IntlProvider } from '@root/modules/intl';
import { store } from './store';
import { App } from './App';

ReactDOM.render(
    <StoreProvider store={store}>
        <IntlProvider>
            <App />
        </IntlProvider>
    </StoreProvider>,
    document.getElementById('root')
);

const hmr = import.meta;
if (hmr.hot) {
    hmr.hot.accept();
}
